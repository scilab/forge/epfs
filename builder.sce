//   This file is part of Epfs.

//    Epfs is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Epfs is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

//    Copyright (c) 2010, Baozeng Ding.
mode(-1);
lines(0);
try
	getversion("scilab");
catch
	error(gettext("Scilab 5.0 or more is required."));
end;
// ====================================================================
if ~with_module("development_tools") then
  error(msprintf(gettext("%s module not installed."),"development_tools"));
end
// ====================================================================
TOOLBOX_NAME = "epfs";
TOOLBOX_TITLE = "Embedding Python From Scilab";
// ====================================================================
toolbox_dir = get_absolute_file_path("builder.sce");
numpy_is_avail=unix_g("python "+toolbox_dir+"test_numpy.py");
tbx_builder_macros(toolbox_dir);
tbx_builder_src(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;
// ====================================================================
