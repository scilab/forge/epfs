"""
    This file is part of Epfs.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Baozeng Ding.
"""

'''py_demo.py - Python source designed to demonstrate the use of embedding python from scilab'''
import matplotlib.pyplot as plt
import numpy as np

def sin():
	x = np.arange(-np.pi, np.pi, 0.01)
	y = np.sin(x)
	plt.plot(x, y, 'g')
	plt.savefig("sin.png", dpi=75)
	print "A beautiful picture saved in ./sin.png !"
	return 6
