/*
    This file is part of Sciscipy and has been updated for Epfs purposes.

    Sciscipy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sciscipy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2009, Vincent Guffens, Baozeng Ding.
*/
#ifdef NUMPY
#ifndef _UTIL_H
#define _UTIL_H

#define PY_ARRAY_UNIQUE_SYMBOL UNIQUE

typedef double complex[2] ;
extern int useNumpy;
#endif
#endif
