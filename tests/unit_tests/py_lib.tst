//   This file is part of Epfs.

//    Epfs is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Epfs is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

//    Copyright (c) 2010, Baozeng Ding.

// import a python standard library module named 'math'
import math;

// test the 'exp' function
if unwrap(math.exp(1)) <> %e then pause, end;

// test the 'log' function
if unwrap(math.log(%e)) <> 1 then pause, end;

// test the 'pow' function
if unwrap(math.pow(2, 3)) <> 8 then pause, end;

// test the 'sqrt' function
if unwrap(math.sqrt(25)) <> 5 then pause, end;

// test the 'asin' function
if unwrap(math.asin(1)) <> %pi / 2 then pause, end;

// test the 'sin' function
if unwrap(math.sin(%pi / 2)) <> 1 then pause, end;

// test the  field 'pi'
if unwrap(math.pi()) <> %pi  then pause, end;

// test the  field 'e'
if unwrap(math.e()) <> %e  then pause, end;

// test useNumpy
import random;
useNumpy(%F);
X = wrap([1,2,3,4]);
random.shuffle(X);
if unwrap(X) == [1, 2, 3, 4] then pause, end;
remove(X);
useNumpy(%T);
X = wrap([1,2,3,4]);
random.shuffle(X);
if unwrap(X) <> [1,2,3,4] then pause, end;

