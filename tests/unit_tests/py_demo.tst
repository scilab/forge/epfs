//   This file is part of Epfs.

//    Epfs is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Epfs is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

//    Copyright (c) 2010, Baozeng Ding.

// import a module named 'py_demo'
import py_demo;

// run the demo function, which saves a figure and return a value.
if unwrap(py_demo.sin()) <> 6 then pause, end;

// remove the module
remove(py_demo);

// clear the variable in scilab
clear('py_demo');

// import the demo module again
import py_demo;

// run the demo function again
if unwrap(py_demo.sin()) <> 6 then pause, end;
