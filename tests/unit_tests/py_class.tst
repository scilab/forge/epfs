//   This file is part of Epfs.

//    Epfs is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Epfs is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

//    Copyright (c) 2010, Baozeng Ding.

// import a module named 'py_class'
import py_class;

// generate the class instance
A = py_class.Multiply();

// test the method
if unwrap(A.multiply()) <> 48 then pause, end;
if unwrap(A.multiply2(6, 10)) <> 60 then pause, end;

// test double matrix
for i = 1:10,
	for j = 1:10,
		a = rand(i,j);
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test int8 matrix
for i=1:10,
	for j=1:10,
		a=int8(10*rand(i,j));
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test int16 matrix
for i = 1:10,
	for j = 1:10,
		a = int16(10*rand(i,j));
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test int32 matrix
for i = 1:10,
	for j = 1:10,
		a = int32(10*rand(i,j));
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test uint8 matrix
for i=1:10,
	for j=1:10,
		a=uint8(10*rand(i,j));
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test uint16 matrix
for i = 1:10,
	for j = 1:10,
		a = uint16(10*rand(i,j));
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test uint32 matrix
for i = 1:10,
	for j = 1:10,
		a = uint32(10*rand(i,j));
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test string matrix
for i = 1:10,
	for j = 1:10,
		a = string(rand(i,j));
		if unwrap(A.F(a)) <> a then pause, end;
	end;
end

// test getField and setField function
if unwrap(A.a()) <> 6 then pause, end;
setField(A, "a", 20);
if unwrap(getField(A, "a")) <> 20 the pause, end;
if getField_u(A, "a") <> 20 then pause, end;

// test invoke function
if invoke_u(A, "F", 6) <> 6 then pause, end;
