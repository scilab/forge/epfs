//    This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
//     has been updated for Epfs purposes.

//    Epfs is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Epfs is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

//    Copyright (c) 2010, Calixte Denizet, Baozeng Ding

function %_PObject_p(obj)
	disp(unwrap(obj));
endfunction
