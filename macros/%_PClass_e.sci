//    This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
//     has been updated for Epfs purposes.

//    Epfs is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Epfs is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

//    Copyright (c) 2010, Calixte Denizet, Baozeng Ding

function y = %_PClass_e(name, M)
	id = M._id;
	if isField(double(id), name) then
		sid = string(id);
		deff('y = tmpmacro(x)','nargs=argn(2),if nargs>=2 then,error(''aa''),end,if nargs==0 then,y = getField_l('+sid+','''+name+'''),else,setField_l('+sid+','''+name+''',x),abort,end');
		y = tmpmacro;
		clear('tmpmacro');
	else
		sid = string(id);
		deff('y = tmpmacro(varargin)','y = invoke_l('+sid+','''+name+''',varargin)');
		y = tmpmacro;
		clear('tmpmacro');
	end
endfunction
