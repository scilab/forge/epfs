// Copyright (C) 2010 - Baozeng Ding
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
sci_gateway_dir = get_absolute_file_path('builder_gateway.sce');
tbx_builder_gateway_lang('c', sci_gateway_dir);
tbx_build_gateway_loader(['c'], sci_gateway_dir);

clear tbx_builder_gateway_lang tbx_build_gateway_loader;
clear sci_gateway_dir;
