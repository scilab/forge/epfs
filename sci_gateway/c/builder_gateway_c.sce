// Copyright (C) 2010 - Baozeng Ding
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
here=get_absolute_file_path('builder_gateway_c.sce');
include = '-I'+here + '../../include' + ' -g -I/usr/include/python2.6/';
if numpy_is_avail <> '0' then
		include=include+' -DNUMPY';
end
funcList = ["import","sci_import"; "class","sci_class"; "invoke", "sci_invoke"; "invoke_u", "sci_invoke_u";  "invoke_l", "sci_invoke_l"; "invoke_lu", "sci_invoke_lu"; "unwrap", "sci_unwrap"; "remove", "sci_remove"; "isClass", "sci_isClass"; "isFunc", "sci_isFunc"; "getField_l", "sci_getfield_l"; "setField_l", "sci_setfield_l"; "getField", "sci_getfield"; "setField", "sci_setfield"; "isField", "sci_isField"; "getField_u", "sci_getfield_u"; "getField_lu", "sci_getfield_lu"; "init", "sci_init"; "wrap", "sci_wrap"; "wrapvar", "sci_wrapvar"; "useNumpy", "sci_usenumpy"; "pythoncp", "sci_pythoncp"];
funcFiles = ["sci_import.c", "sci_class.c", "sci_invoke.c", "sci_invoke_u.c", "sci_invoke_l.c", "sci_invoke_lu.c", "sci_unwrap.c", "sci_remove.c", "sci_isClass.c", "sci_isFunc", "sci_getfield_l.c", "sci_setfield_l.c", "sci_getfield.c", "sci_setfield.c", "sci_isField.c", "sci_getfield_u.c", "sci_getfield_lu.c", "sci_init.c", "sci_wrap.c", "sci_wrapvar.c", "sci_usenumpy.c", "sci_pythoncp.c"];
tbx_build_gateway('gw_epfs', funcList, funcFiles, ..
				here, ..
				 ['../../src/c/libepfs'], ..
				'-lpython2.6', ..
				include);
clear tbx_build_gateway;
