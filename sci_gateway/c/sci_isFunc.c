/*
    This file is part of Epfs.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Baozeng Ding.
*/
#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"

int sci_isFunc(char *fname)
{
	CheckRhs(2, 2);

	SciErr sciErr;
	double ret;


	int idObj = getSingleInt(1, fname);

	char *funcName = getSingleString(2, fname);
	if (!funcName)
	{
		return 0;
	}

	ret = isFunc(idObj, funcName);
	FREE(funcName);
	sciErr = createMatrixOfDouble(pvApiCtx, Rhs + 1, 1, 1, &ret);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	LhsVar(1) = Rhs + 1;
	return 0;
}
