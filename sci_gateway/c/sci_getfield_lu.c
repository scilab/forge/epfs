/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/
#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"

/*
 * Function getfield is called with  2 arguments: getfield(id, fieldname)
 * - id is the id of a _PObj mlist in a Python object and the id is got.
 * - fieldname is the name of the method.
 */
int sci_getfield_lu(char *fname)
{
	CheckRhs(2, 2);

	int idObj = getSingleInt(1, fname);

	char *fieldName = getSingleString(2, fname);
	if (!fieldName)
	{
		return 0;
	}

	int ret = getfield(idObj, fieldName);
	FREE(fieldName);

	if (unwrap(ret, Rhs + 1))
	{
		removescilabpythonobject(ret);
	}
	else
	{
		createPObject(Rhs + 1, (double)ret);
	}
	LhsVar(1) = Rhs + 1;
	return 0;
}
