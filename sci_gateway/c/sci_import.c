/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/
#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"
extern isInit;
/*
 * import a python module and store the module name and id in the mlist:
 * import(name):
 * -name is the name of the module
 */

int sci_import(char *fname)
{

	if (!isInit)
	{
		Scierror(999, "Before import a module, please use the function init() to init  Epfs first\n");
		return 0;
	}
	CheckRhs(1, 1);

	SciErr err;
	char *moduleName = getSingleString(1, fname);

	if (!moduleName)
	{
		return 0;
	}

	char *name = strrchr(moduleName, '.');
	if (!name)
	{
		name = moduleName;
	}
	else if (name[1] == '\0')
	{
		Scierror(999, "%s: The module name cannot end with a dot\n", fname);
		FREE(moduleName);
		return 0;
	}
	else
	{
		name++;
	}

	int *addr = NULL;
	getVarAddressFromName(pvApiCtx, name, &addr);
	if (addr)
	{
		Scierror(999, "%s: A variable named %s already exists on the stack\n", fname, name);
		FREE(moduleName);
		return 0;
	}


	PyObject * pModule =  PyImport_ImportModule(moduleName);
	if (!pModule)
	{
		PyObject *type, *value, *traceback;
		PyErr_Fetch(&type, &value, &traceback);
		Scierror(999, "Error with the module %s : %s\n", moduleName, PyString_AsString(value));
		FREE(moduleName);
		return 0;
	}

	int ret = newobject(pModule);

	if (!createPModule(name, ret))
	{
		return 0;
	}

	FREE(moduleName);

	return 0;
}
