//   This file is part of Epfs.

//    Epfs is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Epfs is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

//    Copyright (c) 2010, Baozeng Ding.
#include "api_scilab.h"
#include "stack-c.h"
#include "util.h"
#include "deallocator.h"

int sci_usenumpy(char *fname)
{
	#ifdef NUMPY
	CheckRhs(1, 1);

	SciErr err;
	int *addr = NULL;
	int row, col, type;
	int *use_numpy;

	err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = getVarType(pvApiCtx, addr, &type);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	if (type == sci_boolean)
	{
		err = getMatrixOfBoolean(pvApiCtx, addr, &row, &col, &use_numpy);
		if (err.iErr)
		{
			printError(&err, 0);
			return 0;
		}
		usenumpy(*use_numpy);
	}
	else
	{
		Scierror(999, "%s: Wrong type for argument 1: bool expected\n", fname);
		return 0;
	}

	LhsVar(1) = 0;
	#endif
	return 0;
}
