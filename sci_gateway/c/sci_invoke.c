/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/
#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"

/*
 * Function invoke is called with more than 2 arguments : invoke(id,method,varargin)
 * - id is the id of a _PObj mlist in a Python object and the id is got.
 * - method is the name of the method.
 * - varargin is a list containing the arguments.
 */
int sci_invoke(char *fname)
{
	if (Rhs < 2)
	{
		Scierror(999, "%s: Wrong number of arguments : more than 2 arguments expected\n", fname);
		return 0;
	}

	SciErr err;
	int *tmpvar = NULL;
	tmpvar = (int*)MALLOC(sizeof(int) * (Rhs - 1));
	if (!tmpvar)
	{
		Scierror(999,"%s: No more memory.\n", fname);
		return NULL;
	}

	*tmpvar = 0;
	int *addr = NULL;

	err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
	if (err.iErr)
	{
		FREE(tmpvar);
		printError(&err, 0);
		return 0;
	}

	int idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);

	if (idObj == -1)
	{
		FREE(tmpvar);
		return 0;
	}


	char *methName = getSingleString(2, fname);
	if (!methName)
	{
		FREE(tmpvar);
		return 0;
	}

	int *args = NULL;
	args = (int*)MALLOC(sizeof(int) * (Rhs - 2));
	if (!args)
	{
		Scierror(999,"%s: No more memory.\n", fname);
		return NULL;
	}

	int i;
	for (i = 0; i < Rhs - 2; i++)
	{
		err = getVarAddressFromPosition(pvApiCtx, i + 3, &addr);
		if (err.iErr)
		{
			FREE(args);
			FREE(tmpvar);
			printError(&err, 0);
			return 0;
		}
		args[i] = getIdOfArg(addr, fname, tmpvar, 0, i + 3);
		if (args[i] == - 1)
		{
			FREE(args);
			FREE(tmpvar);
			return 0;
		}
	}


	int ret = invoke(idObj, methName, args, Rhs - 2);
	FREE(args);
	FREE(methName);
	for (i = 1; i <= tmpvar[0]; i++)
	{
		removescilabpythonobject(tmpvar[i]);
	}
	FREE(tmpvar);
	if (ret < 0)
	{
		LhsVar(1) = Rhs;
		return 0;
	}
	createPObject(Rhs + 1, (double)ret);
	LhsVar(1) = Rhs + 1;
	return 0;
}
