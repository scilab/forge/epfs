/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/
#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"

/*
 * Call the python's class constructor and generate a class instance:
 * loadClass is called with more than 2 arguments: class(id, name, varargin)
 * -id is the id of the module
 * -name is the name of the class
 * -varargin is a list containing the arguments
 */
int sci_class(char *fname)
{
	CheckRhs(3, 3);

	SciErr err;
	int *addr = NULL;
	int typ;

	int idObj = getSingleInt(1, fname);

	char *className = getSingleString(2, fname);
	if (!className)
	{
		return 0;
	}

	err = getVarAddressFromPosition(pvApiCtx, 3, &addr);
	if (err.iErr)
	{
		FREE(className);
		printError(&err, 0);
		return 0;
	}

	err = getVarType(pvApiCtx, addr, &typ);
	if (err.iErr)
	{
		FREE(className);
		printError(&err, 0);
		return 0;
	}

	if (typ != sci_list)
	{
		FREE(className);
		Scierror(999, "%s: Wrong type for input argument %i : List expected\n", fname, 3);
		return NULL;
	}

	int len;
	err = getListItemNumber(pvApiCtx, addr, &len);
	if (err.iErr)
	{
		FREE(className);
		printError(&err, 0);
		return 0;
	}

	int *tmpvar = NULL;
	tmpvar = (int*)MALLOC(sizeof(int) * (len + 1));
	if (!tmpvar)
	{
		FREE(className);
		Scierror(999,"%s: No more memory.\n", fname);
		return NULL;
	}

	*tmpvar = 0;
	int *args = NULL;
	args = (int*)MALLOC(sizeof(int) * len);
	if (!args)
	{
		FREE(className);
		FREE(tmpvar);
		Scierror(999,"%s: No more memory.\n", fname);
		return NULL;
	}

	int *child = NULL;
	int i;
	for (i = 0; i < len; i++)
	{
		err = getListItemAddress(pvApiCtx, addr, i + 1, &child);
		if (err.iErr)
		{
			FREE(args);
			FREE(tmpvar);
			FREE(className);
			printError(&err, 0);
			return 0;
		}
		args[i] = getIdOfArg(child, fname, tmpvar, 0, i + 1);
		if (args[i] == - 1)
		{
			FREE(args);
			FREE(tmpvar);
			FREE(className);
			return 0;
		}
	}
	PyObject * class = loadClass(idObj, className, args, len);
	if (!class)
	{
		FREE(className);
		return 0;
	}

	int ret = newobject(class);
	createPClass(Rhs + 1, (double) ret);
	LhsVar(1) = Rhs + 1;

	FREE(className);
	return 0;
}
