/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/
#include "api_scilab.h"
#include "stack-c.h"

/*
 * This function converts the Scilab objects to the Python objects.
 */
int sci_wrap(char *fname)
{
	if (Rhs == 0)
	{
		Scierror(999,"%s: Wrong number of input arguments: 1 or more expected\n", fname);
		return 0;
	}

	CheckLhs(Rhs, Rhs);

	SciErr err;
	int tmpvar[] = {0, 0};
	int *addr = NULL;
	int i, id;

	for (i = 1; i < Rhs + 1; i++)
	{
		err = getVarAddressFromPosition(pvApiCtx, i, &addr);
		if (err.iErr)
		{
			printError(&err, 0);
			return 0;
		}

		id = getIdOfArg(addr, fname, tmpvar, 0, i);
		*tmpvar = 0;
		if (id == - 1)
		{
			return 0;
		}
		createPObject(Rhs + i, (double) id);
		LhsVar(i) = Rhs + i;
	}

	return 0;
}
