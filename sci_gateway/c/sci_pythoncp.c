/*
    This file is part of Epfs.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Baozeng Ding.
*/
#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"

int sci_pythoncp(char *fname)
{
	CheckRhs(0, 1);

	if (Rhs == 0)
	{
		SciErr sciErr;
		char *path;

		path = pythoncp(NULL);
		sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, 1, 1, &path);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		LhsVar(1) = Rhs + 1;
	}
	else
	{
		char *classPath = getSingleString(1, fname);
		if (!classPath)
		{
			return 0;
		}

		pythoncp(classPath);
		FREE(classPath);
		LhsVar(1) = 0;
	}

	return 0;
}
