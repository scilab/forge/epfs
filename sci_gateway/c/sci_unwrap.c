/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/
#include "api_scilab.h"
#include "stack-c.h"

/*
 * This function converts the Python object to the Scilab object.
 * It takes one argument, which is a mlist containing the Python object.
 */
int sci_unwrap(char *fname)
{
	CheckRhs(1, 1);

	SciErr err;
	int *addr = NULL;
	int row, col, type, idObj;
	double *id;

	err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = getVarType(pvApiCtx, addr, &type);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	if (type != sci_mlist)
	{
		Scierror(999, "%s: Wrong type for argument 1 : _PObj expected\n", fname);
		return 0;
	}

	err = getMatrixOfDoubleInList(pvApiCtx, addr, 2, &row, &col, &id);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	idObj = (int)*id;
	if (unwrap(idObj, Rhs + 1))
	{
		LhsVar(1) = Rhs + 1;
	}
	else
	{
		LhsVar(1) = Rhs;
	}
	return 0;
}
