/*
    This file is part of Sciscipy and has been updated for Epfs purposes.

    Sciscipy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sciscipy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2009, Vincent Guffens, Baozeng Ding.
*/

/*
  Types defined in scilab:

  1  : real or complex constant matrix.
  2  : polynomial matrix.
  4  : boolean matrix.
  5  : sparse matrix.
  6  : sparse boolean matrix.
  8  : matrix of integers stored on 1 2 or 4 bytes
  9  : matrix of graphic handles
  10 : matrix of character strings.
  11 : un-compiled function.
  13 : compiled function.
  14 : function library.
  15 : list.
  16 : typed list (tlist)
  17 : mlist
  128 : pointer


 NOTE: the way numpy vectors and matrices are created leads
        to a memory leak.
*/
#ifdef NUMPY
#include "util.h"
#include "deallocator.h"
#include "api_scilab.h"

PyObject * wrapDoubleArray(double *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	double *mattmp;

	mattmp = (double *)malloc(sizeof(double) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(double) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_DOUBLE),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}


PyObject * wrapIntArray(int *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	int *mattmp;

	mattmp = (int *)malloc(sizeof(int) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(int) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_INT),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}

PyObject * wrapUIntArray(unsigned int *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	unsigned int *mattmp;

	mattmp = (unsigned int *)malloc(sizeof(unsigned int) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(unsigned int) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_UINT),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}

PyObject * wrapByteArray(char *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	char *mattmp;

	mattmp = (char *)malloc(sizeof(char) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(char) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_BYTE),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}

PyObject * wrapUByteArray(unsigned char *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	unsigned char *mattmp;

	mattmp = (unsigned char *)malloc(sizeof(unsigned char) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(unsigned char) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_UBYTE),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}

PyObject * wrapShortArray(short *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	short *mattmp;

	mattmp = (short *)malloc(sizeof(short) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(short) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_SHORT),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}

PyObject * wrapUShortArray(unsigned short *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	unsigned short *mattmp;

	mattmp = (unsigned short *)malloc(sizeof(unsigned short) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(unsigned short) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_USHORT),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}

#ifdef __SCILAB_INT64__
PyObject * wrapLongArray(long long *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	long long *mattmp;

	mattmp = (long long *)malloc(sizeof(long long) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(long long) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_LONG),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}

PyObject * wrapULongArray(unsigned long long *mat, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	dim[0] = m ;
	dim[1] = n ;
	unsigned long long *mattmp;

	mattmp = (unsigned long long *)malloc(sizeof(unsigned long long) * m * n);
	if (!mattmp)
	{
		Scierror(999, "Out of memory.\n");
		return NULL;
	}
	memcpy(mattmp, mat, sizeof(unsigned long long) * m * n);
	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_ULONG),\
					2,\
					dim,\
					NULL,\
					(void *) mattmp,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);
	attach_deallocator(array, mattmp);
	return array ;
}
#endif

PyObject * wrapCDoubleArray(double *cx_real, double *cx_img, int m, int n)
{
	PyObject *array ;
	npy_intp dim[2];
	int i, j;
	complex * cxtmp_transpose;

	cxtmp_transpose = (complex*) malloc(m * n * sizeof(complex));
	if (!cxtmp_transpose)
	{
		Scierror(999, "Out of memory\n");
		return NULL;
	}

	dim[0] = m ;
	dim[1] = n ;
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			cxtmp_transpose[i * n + j][0] = cx_real[i * n + j] ;
			cxtmp_transpose[i * n + j][1] = cx_img[i * n + j] ;
		}
	}

	array = PyArray_NewFromDescr(&PyArray_Type,\
					PyArray_DescrFromType(PyArray_CDOUBLE),\
					2,\
					dim,\
					NULL,\
					(void *) cxtmp_transpose,\
					NPY_OWNDATA | NPY_FARRAY,\
					NULL\
					);

	attach_deallocator(array, cxtmp_transpose);
	return array ;
}
#endif
