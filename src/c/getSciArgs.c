/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/

#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"

#ifdef NUMPY
	#include "util.h"
#endif


/*
 * get a scilab single string
 */
char *getSingleString(int pos, char *fname)
{
	SciErr err;
	int *addr = NULL;
	int typ = 0, row, col, len;
	char *str = NULL;

	err = getVarAddressFromPosition(pvApiCtx, pos, &addr);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}

	err = getVarType(pvApiCtx, addr, &typ);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}

	if (typ != sci_strings)
	{
		Scierror(999, "%s: Wrong type for input argument %i : String expected\n", fname, pos);
		return NULL;
	}

	err = getVarDimension(pvApiCtx, addr, &row, &col);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}

	if (row != 1 || col != 1)
	{
		Scierror(999, "%s: Wrong size for input argument %i : Single string expected\n", fname, pos);
		return NULL;
	}

	err = getMatrixOfString(pvApiCtx, addr, &row, &col, &len, NULL);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}

	str = (char*)MALLOC(len + 1);
	if (!str)
	{
		Scierror(999,"%s: No more memory.\n", fname);
		return NULL;
	}

	err = getMatrixOfString(pvApiCtx, addr, &row, &col, &len, &str);
	if (err.iErr)
	{
		FREE(str);
		printError(&err, 0);
		return NULL;
	}

	return str;
}

/*
 * get a scilab single int
 */
int getSingleInt(int pos, char *fname)
{
	SciErr err;
	int *addr = NULL;
	int typ = 0, row, col;
	double *d = NULL;

	err = getVarAddressFromPosition(pvApiCtx, pos, &addr);
	if (err.iErr)
	{
		printError(&err, 0);
		return -1;
	}

	err = getVarType(pvApiCtx, addr, &typ);
	if (err.iErr)
	{
		printError(&err, 0);
		return -1;
	}

	if (typ != sci_matrix)
	{
		Scierror(999, "%s: Wrong type for input argument %i : Double expected\n", fname, pos);
		return -1;
	}

	if (isVarComplex(pvApiCtx, addr))
	{
		Scierror(999, "%s: Wrong type for input argument %i : Real expected\n", fname, pos);
		return -1;
	}

	err = getVarDimension(pvApiCtx, addr, &row, &col);
	if (err.iErr)
	{
		printError(&err, 0);
		return -1;
	}

	if (row != 1 || col != 1)
	{
		Scierror(999, "%s: Wrong size for input argument %i : Single double expected\n", fname, pos);
		return -1;
	}

	err = getMatrixOfDouble(pvApiCtx, addr, &row, &col, &d);
	if (err.iErr)
	{
		printError(&err, 0);
		return -1;
	}

	return (int)*d;
}

/*
 * get a scilab matrix string
 */
char **getStrings(int *addr, int *row, int *col)
{
	SciErr err;
	int i, rc;
	int *len = NULL;
	char **mat = NULL;

	err = getVarDimension(pvApiCtx, addr, row, col);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}

	rc = *row * *col;
	len = (int*)MALLOC(sizeof(int) * rc);
	if (!len)
	{
		Scierror(999,"No more memory.\n");
		return NULL;
	}

	err = getMatrixOfString(pvApiCtx, addr, row, col, len, NULL);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}

	mat = (char**)MALLOC(sizeof(char*) * rc);
	if (!mat)
	{
		Scierror(999,"No more memory.\n");
		return NULL;
	}

	for (i = 0; i < rc ; i++)
	{
		mat[i] = NULL;
		mat[i] = (char*)MALLOC(sizeof(char) * (len[i] + 1));
		if (!mat[i])
		{
			Scierror(999,"No more memory.\n");
			return NULL;
		}
	}

	err = getMatrixOfString(pvApiCtx, addr, row, col, len, mat);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}

	FREE(len);

	return mat;
}

/*
 * every arg has a id, this function gets the id of the arg
 */
int getIdOfArg(int *addr, char *fname, int *tmpvars, char isClass, int pos)
{
	SciErr err;
	int row, col, returnId = -1;
	int typ = 0;
	PyObject *obj;

	err = getVarType(pvApiCtx, addr, &typ);
	if (err.iErr)
	{
		printError(&err, 0);
		return -1;
	}

	if (isClass && typ != sci_mlist)
	{
		SciError(999,"%s: Wrong type for input argument %i : _PClass is expected\n", fname, pos);
		return -1;
	}

	if (typ == sci_tlist)
	{
		obj = wrapList(addr);
	}
	else
	{
		if (typ == sci_mlist)
		{
			double *id;			
			err = getMatrixOfDoubleInList(pvApiCtx, addr, 2, &row, &col, &id);
			if (err.iErr)
			{
				printError(&err, 0);
				return -1;			
			}
			return (int)id[0];
		}
		else
		{
			obj = convArg(addr);
		}
	}
	returnId = newobject(obj);
	tmpvars[++tmpvars[0]] = returnId;
	return returnId;
}
