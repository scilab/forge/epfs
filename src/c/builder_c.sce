// Copyright (C) 2010 - Baozeng Ding
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
src_c_path = get_absolute_file_path("builder_c.sce");
include = '-I' + src_c_path + '../../include' + ' -g -I/usr/include/python2.6/';
if numpy_is_avail <> '0' then
		include=include+' -DNUMPY';
end
tbx_build_src('epfs', ['deallocator.c', 'getSciArgs.c', 'ScilabObjects.c', 'ScilabPythonObject.c', 'unwrapArray.c', 'unwrapDict.c', 'unwrapDictArray.c', 'wrapArray.c'], 'c', ..
			src_c_path, ..
			'', ..
			'-lpython2.6',include);
clear tbx_build_src;
