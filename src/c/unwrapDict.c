/*
    This file is part of Epfs.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Baozeng Ding.
*/

#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"
#ifdef NUMPY
#include "util.h"
#include "deallocator.h"
#endif

int createdouble(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	double res;
	SciErr sciErr;

	res = PyFloat_AsDouble(obj);
	sciErr = createMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, 1, 1, (double *)&res);

	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int createcdouble(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	double cx_real = 0;
	double  cx_img = 0;
	SciErr sciErr;

	cx_real = PyComplex_RealAsDouble(obj);
	cx_img =  PyComplex_ImagAsDouble(obj);
	sciErr = createComplexMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, 1, 1, &cx_real, &cx_img);

	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int createint(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	double res;
	SciErr sciErr;

	res = (double)PyInt_AsLong(obj);
	sciErr = createMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, 1, 1, (double *)&res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

#ifdef ____SCILAB_INT64__
int createlong(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	long long res;
	SciErr sciErr;

	res = (long long)PyLong_AsLongLong(obj);
	sciErr = createMatrixOfInteger64InList(pvApiCtx, pos, tlist_addr, itemPos, 1, 1, (long long *)&res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}
#endif

int createstring(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	char *res;
	SciErr sciErr;

	res = PyString_AsString(obj);
	sciErr = createMatrixOfStringInList(pvApiCtx, pos, tlist_addr, itemPos, 1, 1, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int createrowdouble(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	double *res;
	int n, i;
	SciErr sciErr;

	n = PyList_Size(obj);
	sciErr = allocMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, 1, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		res[i] = PyFloat_AsDouble(PyList_GetItem(obj, i));
	}

	return 0;
}

int createrowcdouble(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	double *cx_real, *cx_img;
	int n, i;
	SciErr sciErr;
	PyObject *item;

	n = PyList_Size(obj);
	sciErr = allocComplexMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, 1, n, &cx_real, &cx_img);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		item = PyList_GetItem(obj, i);
		cx_real[i] = PyComplex_RealAsDouble(item);
		cx_img[i] = PyComplex_ImagAsDouble(item);
	}

	return 0;
}

int createrowint(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	double *res;
	int n, i;
	SciErr sciErr;

	n = PyList_Size(obj);
	sciErr = allocMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, 1, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		res[i] = (double)PyInt_AsLong(PyList_GetItem(obj, i));
	}

	return 0;
}

#ifdef ____SCILAB_INT64__
int createrowlong(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	long long *res;
	int n, i;
	SciErr sciErr;

	n = PyList_Size(obj);
	sciErr = allocMatrixOfInteger64InList(pvApiCtx, pos, tlist_addr, itemPos, 1, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		res[i] = PyLong_AsLongLong(PyList_GetItem(obj, i));
	}

	return 0;
}
#endif

int createrowstring(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	char **res;
	int n, i;
	SciErr sciErr;

	n = PyList_Size(obj);
	res = (char **)MALLOC(sizeof(char *) * n);

	for (i = 0; i < n; i++)
	{
		PyObject *item = PyList_GetItem(obj, i);
		res[i] = PyString_AsString(item);
	}

	sciErr = createMatrixOfStringInList(pvApiCtx, pos, tlist_addr, itemPos, 1, n, (char **)res);
	FREE(res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int creatematdouble(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	#ifdef NUMPY
	unwrapDoubleDictArray(pos, tlist_addr, itemPos, obj);
	#else
	double *res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *item;

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);
	sciErr = allocMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = PyFloat_AsDouble(PyList_GetItem(item, j));
		}
	}
	#endif
	return 0;
}

int creatematcdouble(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	double *cx_real, *cx_img;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *row_list, *item;

	m = PyList_Size(obj);
	row_list = PyList_GetItem(obj, 0);
	n = PyList_Size(row_list);
	sciErr = allocComplexMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, m, n, &cx_real, &cx_img);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		row_list = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			item = PyList_GetItem(row_list, j);
			cx_real[j * m + i] = PyComplex_RealAsDouble(item);
			cx_img[j * m + i] = PyComplex_ImagAsDouble(item);
		}
	}

	return 0;
}

int creatematint(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	#ifdef NUMPY
	unwrapIntDictArray(pos, tlist_addr, itemPos, obj);
	#else
	double *res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *item;

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);
	sciErr = allocMatrixOfDoubleInList(pvApiCtx, pos, tlist_addr, itemPos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = (double)PyInt_AsLong(PyList_GetItem(item, j));
		}
	}
	#endif
	return 0;
}

#ifdef ____SCILAB_INT64__
int creatematlong(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	#ifdef NUMPY
	unwrapLongDictArray(pos, tlist_addr, itemPos, obj);
	#else
	long long *res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *item;

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);
	sciErr = allocMatrixOfInteger64InList(pvApiCtx, pos, tlist_addr, itemPos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = PyLong_AsLongLong(PyList_GetItem(item, j));
		}
	}
	#endif
	return 0;
}
#endif

int creatematstring(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	char **res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *item;

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);

	res = (char **)MALLOC(sizeof(char *) * m * n);
	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = PyString_AsString(PyList_GetItem(item, j));
		}
	}

	sciErr = createMatrixOfStringInList(pvApiCtx, pos, tlist_addr, itemPos, m, n, (char **)res);
	FREE(res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}



#ifdef NUMPY
int creatematuint(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	unwrapUIntDictArray(pos, tlist_addr, itemPos, obj);
	return 0;
}

int creatematbyte(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	unwrapByteDictArray(pos, tlist_addr, itemPos, obj);
	return 0;
}

int creatematubyte(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	unwrapUByteDictArray(pos, tlist_addr, itemPos, obj);
	return 0;
}

int creatematshort(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	unwrapShortDictArray(pos, tlist_addr, itemPos, obj);
	return 0;
}

int creatematushort(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	unwrapUShortDictArray(pos, tlist_addr, itemPos, obj);
	return 0;
}

#ifdef ____SCILAB_INT64__
int creatematulong(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	unwrapULongDictArray(pos, tlist_addr, itemPos, obj);
	return 0;
}
#endif

#endif
