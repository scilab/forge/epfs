/*
    This file is part of Sciscipy and has been updated for Epfs purposes.

    Sciscipy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sciscipy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2009, Vincent Guffens, Baozeng Ding.
*/
#ifdef NUMPY
#include <Python.h>
#include "util.h"
#include "deallocator.h"
#include "api_scilab.h"

int unwrapDoubleArray(int pos, PyObject *obj)
{
	double *res, *cx_real, *cx_img;
	int m, n, i, j, row_stride, col_stride;
	complex *item;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	if (array->descr->type_num == PyArray_CDOUBLE)
	{
		sciErr = allocComplexMatrixOfDouble(pvApiCtx, pos, m, n, &cx_real, &cx_img);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				item = (complex*)(array->data + i * row_stride+ j * col_stride);
				cx_real[j * m + i] = (*item)[0];
				cx_img[j * m + i] = (*item)[1];
			}
		}
		return 0;
	}

	sciErr = allocMatrixOfDouble(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(double) && col_stride == (sizeof(double) * m))
	{
		memcpy(res, (double *)array->data, sizeof(double) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(double *)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

int unwrapIntArray(int pos, PyObject *obj)
{
	int *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfInteger32(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(int) && col_stride == (sizeof(int) * m))
	{
		memcpy(res, (int *)array->data, sizeof(int) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(int *)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

int unwrapUIntArray(int pos, PyObject *obj)
{
	unsigned int *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfUnsignedInteger32(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(unsigned int) && col_stride == (sizeof(unsigned int) * m))
	{
		memcpy(res, (unsigned int *)array->data, sizeof(unsigned int) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(unsigned int *)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

int unwrapByteArray(int pos, PyObject *obj)
{
	char *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfInteger8(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(char) && col_stride == (sizeof(char) * m))
	{
		memcpy(res, (char *)array->data, sizeof(char) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(char *)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

int unwrapUByteArray(int pos, PyObject *obj)
{
	unsigned char *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfUnsignedInteger8(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(unsigned char) && col_stride == (sizeof(unsigned char) * m))
	{
		memcpy(res, (unsigned char *)array->data, sizeof(unsigned char) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(unsigned char *)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

int unwrapShortArray(int pos, PyObject *obj)
{
	short *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfInteger16(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(short) && col_stride == (sizeof(short) * m))
	{
		memcpy(res, (short *)array->data, sizeof(short) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(short *)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

int unwrapUShortArray(int pos, PyObject *obj)
{
	unsigned short *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfUnsignedInteger16(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(unsigned short) && col_stride == (sizeof(unsigned short) * m))
	{
		memcpy(res, (unsigned short *)array->data, sizeof(unsigned short) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(unsigned short *)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

#ifdef __SCILAB_INT64__
int unwrapLongArray(int pos, PyObject *obj)
{
	long long *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfInteger64(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(long long) && col_stride == (sizeof(long long) * m))
	{
		memcpy(res, (long long *)array->data, sizeof(long long) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(long long*)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}

int unwrapULongArray(int pos, PyObject *obj)
{
	unsigned long long *res;
	int m, n, i, j, row_stride, col_stride;
	SciErr sciErr;

	PyArrayObject * array = (PyArrayObject *) obj;
	if (array->nd != 1 && array->nd != 2)
	{
		Scierror(999, "Only 1D and 2D array are supported!\n");
		return -1;
	}

	m = array->dimensions[0];
	n = array->dimensions[1];
	row_stride = array->strides[0];
	col_stride = array->strides[1];

	sciErr = allocMatrixOfUnsignedInteger64(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (row_stride == sizeof(unsigned long long) && col_stride == (sizeof(unsigned long long) * m))
	{
		memcpy(res, (unsigned long long *)array->data, sizeof(unsigned long long) * m * n);
	}
	else
	{
		for (i = 0; i < m; i++)
		{
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = *(unsigned long long*)(array->data + i * row_stride + j * col_stride) ;
			}
		}
	}
	return 0;
}
#endif

#endif
