/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/

#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"
#ifdef NUMPY
#include "util.h"
#include "deallocator.h"
int useNumpy = 1;
#endif

#define SINGLE_DOUBLE 0
#define SINGLE_INT 1
#define SINGLE_LONG 2
#define SINGLE_STRING 3
#define ROW_DOUBLE 4
#define ROW_INT 5
#define ROW_LONG 6
#define ROW_STRING 7
#define MAT_DOUBLE 8
#define MAT_INT 9
#define MAT_LONG 10
#define MAT_STRING 11
#define SINGLE_CDOUBLE 12
#define ROW_CDOUBLE 13
#define MAT_CDOUBLE 14
#define UINT_ARRAY 15
#define BYTE_ARRAY 16
#define UBYTE_ARRAY 17
#define SHORT_ARRAY 18
#define USHORT_ARRAY 19
#define ULONG_ARRAY 20
#define PY_DICT 21

static int currentCapacity = 1024;
static int currentFreeCapacity = 1;
static PyObject * ScilabPythonObject = NULL;
static int id = 0;
static int* freeList;
static int front = 0;
static int rear = 0;
extern int isInit = 0;

/*
 * initialize the python runtime environment
 */
int py_initialize()
{
	if (isInit == 1)
	{
		Scierror(999, "Epfs has been initialized already!\n");
		return 0;
	}
	isInit = 1;
	Py_Initialize();
	#ifdef NUMPY
	import_array();
	_MyDeallocType.tp_new = PyType_GenericNew ;
	if (PyType_Ready(&_MyDeallocType) < 0)
	{
		Scierror(999, "Can not initialize deallocator\n");
	}
	Py_INCREF(&_MyDeallocType);
	#endif
	PyRun_SimpleString("import sys");
	PyRun_SimpleString("sys.path.append('./')");
	PyRun_SimpleString("sys.path.append('./classpath')");

	ScilabPythonObject = PyList_New(currentCapacity);
	if (!ScilabPythonObject)
	{
		Scierror(999, "Error when allocing memory for PyList\n");
		return 0;
	}
	freeList = (int *)MALLOC(sizeof(int) * currentFreeCapacity);
	if (!freeList)
	{
		Scierror(999, "Not enough memory\n");
	}
	memset(freeList, 0, currentFreeCapacity * sizeof(int));

	return 1;
}

/*
 * add python class directory, so python could find the classes.
 * if no args, it would return the python system classpath.
 */
char * pythoncp(char * classpath)
{
	if (!classpath)
	{
		char *sysPath = Py_GetPath();
		return sysPath;
	}
	else
	{
		char *addPath = (char *)MALLOC(strlen(classpath) + 20);
		if (!addPath)
		{
			Scierror(999, "Not enough memory\n");
		}

		memset(addPath, '\0', strlen(classpath) + 20);
		strcpy(addPath, "sys.path.append(\'");
		strcpy(addPath + strlen("sys.path.append(\'"), classpath);
		strcpy(addPath + strlen("sys.path.append(\'") + strlen(classpath), "\')");
		PyRun_SimpleString(addPath);

		FREE(addPath);
		return 1;
	}
}

#ifdef NUMPY
int usenumpy(int x)
{
	if (x)
	{
		useNumpy = 1;
	}
	else
	{
		useNumpy = 0;
	}
}
#endif

/*
 * add a new ScilabPyObject
 */
int newobject(PyObject * obj)
{
	PyObject *new_list;
	int i, idObj;
	if (front != rear)
	{
		idObj = freeList[front];
		front++;
		if (front == currentFreeCapacity)
		{
			front = 0;
		}
	}
	else
	{
		if (id < currentCapacity)
		{
			idObj = id;
			id++;
		}
		else
		{
			currentCapacity = currentCapacity * 2;
			new_list = PyList_New(currentCapacity);
			if (!new_list)
			{
				Scierror(999, "Error when allocing memory for PyList\n");
				return -1;
			}
			for (i = 0; i < currentCapacity / 2; i++)
			{
				PyList_SetItem(new_list, i, PyList_GetItem(ScilabPythonObject, i));
			}

			ScilabPythonObject = new_list;
			idObj = id;
			id++;
		}
	}
	PyList_SetItem(ScilabPythonObject, idObj, obj);
	return idObj;
}

/*
 * remove a ScilabPyObject
 */
int removescilabpythonobject(int idObj)
{
	if (rear + 1 % currentFreeCapacity == front)
	{
		currentFreeCapacity = currentFreeCapacity * 2;
		freeList = realloc(freeList, currentFreeCapacity);
		if (!freeList)
		{
			Scierror(999, "Not enough memory\n");
		}
		freeList[rear] = idObj;
		front = 0;
		rear = currentFreeCapacity / 2;
	}
	else
	{
		freeList[rear++] = idObj;
		if (rear == currentFreeCapacity)
		{
			rear = 0;
		}
	}
	PyList_SetItem(ScilabPythonObject, idObj, NULL);

	return 1;
}

/*
 * This function is to judge whether the module has a class.
 * @param id: the id of the module. 
 * @param classname: the name of the class.
 * @return: 1 if the moudle has the class, else 0
 */
int isClass(int id, char *classname)
{
	int i;
	PyObject *pModule, *pClass;
	pModule = PyList_GetItem(ScilabPythonObject, id);
	if (!pModule)
	{
		return 0;
	}

	pClass = PyObject_GetAttrString(pModule, classname);
	if (!pClass)
	{
		return 0;
	}

	if (!PyClass_Check(pClass))
	{
		return 0;
	}

	return 1;
}


/*
 * This function is to judge whether the module has a function.
 * @param id: the id of the module.
 * @param functionname: the name of the function.
 * @return: 1 if the moudle has the function, else 0
 */
int isFunc(int id, char *funcname)
{
	int i;
	PyObject *pModule, *pFunc;
	pModule = PyList_GetItem(ScilabPythonObject, id);
	if (!pModule)
	{
		return 0;
	}

	pFunc = PyObject_GetAttrString(pModule, funcname);
	if (!pFunc)
	{
		return 0;
	}

	if (PyFunction_Check(pFunc))
	{
		return 1;
	}

	if (PyCFunction_Check(pFunc))
	{
		return 1;
	}

	if (PyMethod_Check(pFunc))
	{
		return 1;
	}
	return 0;
}

/*
 * This function is to judge whether the class instance has a field.
 * @param id: the id of the class instance.
 * @param fieldname: the name of the function.
 * @return : 1 if the classinstance has the filed attr, else 0
 */
int isField(int id, char *fieldname)
{
	int i;
	PyObject *pInstance, *pField;
	pInstance = PyList_GetItem(ScilabPythonObject, id);
	if (!pInstance)
	{
		return 0;
	}

	pField = PyObject_GetAttrString(pInstance, fieldname);
	if (!pField)
	{
		return 0;
	}

	if (PyMethod_Check(pField))
	{
		return 0;
	}

	return 1;
}

/*
 * load the Python class of the module with id
 */
PyObject *loadClass(int id, char * classname, int *args, int len)
{
	int i;
	PyObject *pModule, * pDict, *pClass, *pArgs, *pArg, *pInstance;
	pModule = PyList_GetItem(ScilabPythonObject, id);

	if (!pModule)
	{
		Scierror(999, "Can not find the module with id %i\n", id);
		return 0;
	}

	pDict = PyModule_GetDict(pModule);
	if (!pDict)
	{
		Scierror(999, "Can not find the dictionary\n");
		return 0;
	}

	pClass = PyDict_GetItemString(pDict, classname);
	if (!PyCallable_Check(pClass))
	{
		Scierror(999, "Can not find the class %s\n", classname);
		return 0;
	}
	else
	{
		pArgs = PyTuple_New(len);
		for (i = 0; i < len; i++)
		{
			pArg = PyList_GetItem(ScilabPythonObject, args[i]);
			Py_INCREF(pArg);
			PyTuple_SetItem(pArgs, i, pArg);
		}
		pInstance = PyObject_Call(pClass, pArgs, NULL);
		if (!pInstance)
		{
			Scierror(999, "Error when creating the instance. Please check the args.\n");
			Py_DECREF(pArgs);
			return 0;
		}
		else
		{
			Py_DECREF(pArgs);
			return pInstance;
		}
	}
}

/*
 * invoke the method with the class'id, name of the method, args and the length of the args.
 */
int invoke(int id, char * method, int *args, int len)
{
	int result = 0;
	int i;
	PyObject *pInstance, *pMethod, *pCallable, *pArgs, *pArg, *pRes;

	pInstance = PyList_GetItem(ScilabPythonObject, id);
	if (!pInstance)
	{
		Scierror(999, "Can not find the class instance.\n");
		return -1;
	}

	pMethod = PyString_FromString(method);

	pCallable = PyObject_GetAttr(pInstance, pMethod);
	if (!pCallable)
	{
		Scierror(999, "Can not find the method %s.\n", method);
		Py_DECREF(pMethod);
		return -1;
	}

	pArgs = PyTuple_New(len);
	for (i = 0; i < len; i++)
	{
		pArg = PyList_GetItem(ScilabPythonObject, args[i]);
		Py_INCREF(pArg);
		PyTuple_SetItem(pArgs, i, pArg);
	}

	pRes = PyObject_Call(pCallable, pArgs, NULL);
	if (pRes)
	{
		if (pRes == Py_None)
		{
			Py_DECREF(pArgs);
			return -1;
		}
		result = newobject(pRes);
		Py_DECREF(pArgs);
		return result;
	}
	else
	{
		PyObject *type, *value, *traceback;
		PyErr_Fetch(&type, &value, &traceback);
		Scierror(999, "Error when calling the method %s : %s\n", method, PyString_AsString(value));
		Py_DECREF(pArgs);
		return -1;
	}
}

int getfield(int id, char * fieldname)
{
	int result = 0;
	PyObject *pInstance, *pField, *pRes;

	pInstance = PyList_GetItem(ScilabPythonObject, id);
	if (!pInstance)
	{
		Scierror(999, "Can not find the class instance.\n");
		return -1;
	}

	pField = PyString_FromString(fieldname);

	pRes = PyObject_GetAttr(pInstance, pField);
	if (!pRes)
	{
		Scierror(999, "Can not find the field %s.\n", fieldname);
		Py_DECREF(pField);
		return 0;
	}

	result = newobject(pRes);
	return result;
}

int setfield(int id, char * fieldname, int arg)
{
	int result = 0;
	PyObject *pInstance, *pField, *pArg, *pRes;

	pInstance = PyList_GetItem(ScilabPythonObject, id);
	if (!pInstance)
	{
		Scierror(999, "Can not find the class instance.\n");
		return -1;
	}

	pField = PyString_FromString(fieldname);
	pArg = PyList_GetItem(ScilabPythonObject, arg);
	result = PyObject_SetAttr(pInstance, pField, pArg);
	if (result == -1)
	{
		Scierror(999, "Error when setting the attribute of the field: %s.\n", fieldname);
		Py_DECREF(pField);
		return 0;
	}

	return 1;
}

int isunwrappable(PyObject *obj)
{
	#ifdef NUMPY
	if (useNumpy)
	{
		if (!PyArray_Check(obj))
		{
			if (PyFloat_Check(obj))
			{
				return SINGLE_DOUBLE;
			}
			if (PyInt_Check(obj))
			{
				return SINGLE_INT;
			}
			if (PyLong_Check(obj))
			{
				return SINGLE_LONG;
			}
			if (PyString_Check(obj))
			{
				return SINGLE_STRING;
			}
			if (PyDict_Check(obj))
			{
				return PY_DICT;
			}
			if (PyList_Check(obj))
			{
				int  n = PyList_Size(obj);
				if (n == 0)
				{
					return  -1;
				}

				PyObject *item = PyList_GetItem(obj, 0);
				if (!PyList_Check(item))
				{
					if (PyString_Check(item))
					{
						return ROW_STRING;
					}
					return -1;
				}
				else
				{
					PyObject *el = PyList_GetItem(item, 0);
					n = PyList_Size(el);
					if (n == 0)
					{
						return -1;
					}
					if (PyString_Check(el))
					{
						return MAT_STRING;
					}
					return -1;
				}
			}
			return -1;
		}
		else
		{
			PyArrayObject * array = (PyArrayObject *) obj;
			if (array->descr->type_num == PyArray_DOUBLE || array->descr->type_num == PyArray_CDOUBLE)
			{
				return MAT_DOUBLE;
			}
			if (array->descr->type_num == PyArray_INT)
			{
				return MAT_INT;
			}
			if (array->descr->type_num == PyArray_UINT)
			{
				return UINT_ARRAY;
			}
			if (array->descr->type_num == PyArray_BYTE)
			{
				return BYTE_ARRAY;
			}
			if (array->descr->type_num == PyArray_UBYTE)
			{
				return UBYTE_ARRAY;
			}
			if (array->descr->type_num == PyArray_SHORT)
			{
				return SHORT_ARRAY;
			}
			if (array->descr->type_num == PyArray_USHORT)
			{
				return USHORT_ARRAY;
			}
			if (array->descr->type_num == PyArray_LONG)
			{
				return MAT_LONG;
			}
			if (array->descr->type_num == PyArray_ULONG)
			{
				return ULONG_ARRAY;
			}
			return -1;
		}
	}
	else
	{
		if (!PyList_Check(obj))
		{
			if (PyFloat_Check(obj))
			{
				return SINGLE_DOUBLE;
			}
			if (PyInt_Check(obj))
			{
				return SINGLE_INT;
			}
			if (PyLong_Check(obj))
			{
				return SINGLE_LONG;
			}
			if (PyString_Check(obj))
			{
				return SINGLE_STRING;
			}
			if (PyComplex_Check(obj))
			{
				return SINGLE_CDOUBLE;
			}
			if (PyDict_Check(obj))
			{
				return PY_DICT;
			}
			return -1;
		}
		else
		{
			int  n = PyList_Size(obj);

			if (n == 0)
			{
				return  -1;
			}

			PyObject *item = PyList_GetItem(obj, 0);

			if (!PyList_Check(item))
			{
				if (PyFloat_Check(item))
				{
					return ROW_DOUBLE;
				}
				if (PyInt_Check(item))
				{
					return ROW_INT;
				}
				if (PyLong_Check(item))
				{
					return ROW_LONG;
				}
				if (PyString_Check(item))
				{
					return ROW_STRING;
				}
				if (PyComplex_Check(item))
				{
					return ROW_CDOUBLE;
				}
				return -1;
			}
			else
			{
				PyObject * el = PyList_GetItem(item, 0);
				n = PyList_Size(el);
				if (n == 0)
				{
					return -1;
				}
				if (PyFloat_Check(el))
				{
					return MAT_DOUBLE;
				}
				if (PyInt_Check(el))
				{
					return MAT_INT;
				}
				if (PyLong_Check(el))
				{
					return MAT_LONG;
				}
				if (PyString_Check(el))
				{
					return MAT_STRING;
				}
				if (PyComplex_Check(el))
				{
					return MAT_CDOUBLE;
				}
				return -1;
			}
		}
	}
	#else
	if (!PyList_Check(obj))
	{
		if (PyFloat_Check(obj))
		{
			return SINGLE_DOUBLE;
		}
		if (PyInt_Check(obj))
		{
			return SINGLE_INT;
		}
		if (PyLong_Check(obj))
		{
			return SINGLE_LONG;
		}
		if (PyString_Check(obj))
		{
			return SINGLE_STRING;
		}
		if (PyComplex_Check(obj))
		{
			return SINGLE_CDOUBLE;
		}
		if (PyDict_Check(obj))
		{
			return PY_DICT;
		}
		return -1;
	}
	else
	{
		int  n = PyList_Size(obj);

		if (n == 0)
		{
			return  -1;
		}

		PyObject *item = PyList_GetItem(obj, 0);

		if (!PyList_Check(item))
		{
			if (PyFloat_Check(item))
			{
				return ROW_DOUBLE;
			}
			if (PyInt_Check(item))
			{
				return ROW_INT;
			}
			if (PyLong_Check(item))
			{
				return ROW_LONG;
			}
			if (PyString_Check(item))
			{
				return ROW_STRING;
			}
			if (PyComplex_Check(item))
			{
				return ROW_CDOUBLE;
			}
			return -1;
		}
		else
		{
			PyObject * el = PyList_GetItem(item, 0);
			n = PyList_Size(el);
			if (n == 0)
			{
				return -1;
			}
			if (PyFloat_Check(el))
			{
				return MAT_DOUBLE;
			}
			if (PyInt_Check(el))
			{
				return MAT_INT;
			}
			if (PyLong_Check(el))
			{
				return MAT_LONG;
			}
			if (PyString_Check(el))
			{
				return MAT_STRING;
			}
			if (PyComplex_Check(el))
			{
				return MAT_CDOUBLE;
			}
			return -1;
		}
	}
	#endif
}

int unwrap(int idObj, int pos)
{
	int type;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "The object with the id %i does not exit!\n", idObj);
		return -1;
	}
	type = isunwrappable(obj);
	switch (type)
	{
		case -1:;
			return 0;
		case SINGLE_DOUBLE:;
			unwrapdouble(idObj, pos);
			return 1;
		case SINGLE_INT:;
			unwrapint(idObj, pos);
			return 1;
		case SINGLE_LONG:;
			#ifdef __SCILAB_INT64__
			unwraplong(idObj, pos);
			#endif
			return 1;
		case SINGLE_STRING:;
			unwrapstring(idObj, pos);
			return 1;
		case ROW_DOUBLE:;
			unwraprowdouble(idObj, pos);
			return 1;
		case ROW_INT:;
			unwraprowint(idObj, pos);
			return 1;
		case ROW_LONG:;
			#ifdef __SCILAB_INT64__
			unwraprowlong(idObj, pos);
			#endif
			return 1;
		case ROW_STRING:;
			unwraprowstring(idObj, pos);
			return 1;
		case MAT_DOUBLE:;
			unwrapmatdouble(idObj, pos);
			return 1;
		case MAT_INT:;
			unwrapmatint(idObj, pos);
			return 1;
		case MAT_LONG:;
			#ifdef __SCILAB_INT64__
			unwrapmatlong(idObj, pos);
			#endif
			return 1;
		case MAT_STRING:;
			unwrapmatstring(idObj, pos);
			return 1;
		case SINGLE_CDOUBLE:;
			unwrapcdouble(idObj, pos);
			return 1;
		case ROW_CDOUBLE:;
			unwraprowcdouble(idObj, pos);
			return 1;
		case MAT_CDOUBLE:;
			unwrapmatcdouble(idObj, pos);
			return 1;
		#ifdef NUMPY
		case UINT_ARRAY:;
			unwrapmatuint(idObj, pos);
			return 1;
		case BYTE_ARRAY:;
			unwrapmatbyte(idObj, pos);
			return 1;
		case UBYTE_ARRAY:;
			unwrapmatubyte(idObj, pos);
			return 1;
		case SHORT_ARRAY:;
			unwrapmatshort(idObj, pos);
			return 1;
		case USHORT_ARRAY:;
			unwrapmatushort(idObj, pos);
			return 1;
		case ULONG_ARRAY:;
			#ifdef ____SCILAB_INT64__
			unwrapmatulong(idObj, pos);
			#endif
			return 1;
		#endif
		case PY_DICT:;
			unwrapdict(idObj, pos);
			return 1;
		default:;
			return 0;
	}
}

int unwrapdouble(int idObj, int pos)
{
	double res;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);

	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	res = PyFloat_AsDouble(obj);
	sciErr = createMatrixOfDouble(pvApiCtx, pos, 1, 1, (double *)&res);

	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int unwrapcdouble(int idObj, int pos)
{
	double cx_real = 0;
	double  cx_img = 0;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);

	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	cx_real = PyComplex_RealAsDouble(obj);
	cx_img =  PyComplex_ImagAsDouble(obj);
	sciErr = createComplexMatrixOfDouble(pvApiCtx, pos, 1, 1, &cx_real, &cx_img);

	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int unwrapint(int idObj, int pos)
{
	double res;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	res = (double)PyInt_AsLong(obj);
	sciErr = createMatrixOfDouble(pvApiCtx, pos, 1, 1, (double *)&res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

#ifdef ____SCILAB_INT64__
int unwraplong(int idObj, int pos)
{
	long long res;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	res = (long long)PyLong_AsLongLong(obj);
	sciErr = createMatrixOfInteger64(pvApiCtx, pos, 1, 1, (long long *)&res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}
#endif

int unwrapstring(int idObj, int pos)
{
	char *res;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	res = PyString_AsString(obj);
	sciErr = createMatrixOfString(pvApiCtx, pos, 1, 1, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int unwraprowdouble(int idObj, int pos)
{
	double *res;
	int n, i;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	n = PyList_Size(obj);
	sciErr = allocMatrixOfDouble(pvApiCtx, pos, 1, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		res[i] = PyFloat_AsDouble(PyList_GetItem(obj, i));
	}

	return 0;
}

int unwraprowcdouble(int idObj, int pos)
{
	double *cx_real, *cx_img;
	int n, i;
	SciErr sciErr;

	PyObject *obj, *item;
	obj= PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	n = PyList_Size(obj);
	sciErr = allocComplexMatrixOfDouble(pvApiCtx, pos, 1, n, &cx_real, &cx_img);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		item = PyList_GetItem(obj, i);
		cx_real[i] = PyComplex_RealAsDouble(item);
		cx_img[i] = PyComplex_ImagAsDouble(item);
	}

	return 0;
}

int unwraprowint(int idObj, int pos)
{
	double *res;
	int n, i;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	n = PyList_Size(obj);
	sciErr = allocMatrixOfDouble(pvApiCtx, pos, 1, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		res[i] = (double)PyInt_AsLong(PyList_GetItem(obj, i));
	}

	return 0;
}

#ifdef ____SCILAB_INT64__
int unwraprowlong(int idObj, int pos)
{
	long long *res;
	int n, i;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	n = PyList_Size(obj);
	sciErr = allocMatrixOfInteger64(pvApiCtx, pos, 1, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < n; i++)
	{
		res[i] = PyLong_AsLongLong(PyList_GetItem(obj, i));
	}

	return 0;
}
#endif

int unwraprowstring(int idObj, int pos)
{
	char **res;
	int n, i;
	SciErr sciErr;

	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	n = PyList_Size(obj);
	res = (char **)MALLOC(sizeof(char *) * n);

	for (i = 0; i < n; i++)
	{
		PyObject *item = PyList_GetItem(obj, i);
		res[i] = PyString_AsString(item);
	}

	sciErr = createMatrixOfString(pvApiCtx, pos, 1, n, (char **)res);
	FREE(res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int unwrapmatdouble(int idObj, int pos)
{
	#ifdef NUMPY
	if (useNumpy)
	{
		PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
		unwrapDoubleArray(pos, obj);
	}
	else
	{
		double *res;
		int m, n, i, j;
		SciErr sciErr;
		PyObject *obj, *item;

		obj = PyList_GetItem(ScilabPythonObject, idObj);
		if (!obj)
		{
			Scierror(999, "No such object on the stack!\n");
			return -1;
		}

		m = PyList_Size(obj);
		item = PyList_GetItem(obj, 0);
		n = PyList_Size(item);
		sciErr = allocMatrixOfDouble(pvApiCtx, pos, m, n, &res);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		for (i = 0; i < m; i++)
		{
			item = PyList_GetItem(obj, i);
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = PyFloat_AsDouble(PyList_GetItem(item, j));
			}
		}
	}
	#else
	double *res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *obj, *item;

	obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);
	sciErr = allocMatrixOfDouble(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = PyFloat_AsDouble(PyList_GetItem(item, j));
		}
	}
	#endif
	return 0;
}

int unwrapmatcdouble(int idObj, int pos)
{
	double *cx_real, *cx_img;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *obj, *row_list, *item;

	obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	m = PyList_Size(obj);
	row_list = PyList_GetItem(obj, 0);
	n = PyList_Size(row_list);
	sciErr = allocComplexMatrixOfDouble(pvApiCtx, pos, m, n, &cx_real, &cx_img);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		row_list = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			item = PyList_GetItem(row_list, j);
			cx_real[j * m + i] = PyComplex_RealAsDouble(item);
			cx_img[j * m + i] = PyComplex_ImagAsDouble(item);
		}
	}

	return 0;
}

int unwrapmatint(int idObj, int pos)
{
	#ifdef NUMPY
	if (useNumpy)
	{
		PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
		unwrapIntArray(pos, obj);
	}
	else
	{
		double *res;
		int m, n, i, j;
		SciErr sciErr;
		PyObject *obj, *item;

		obj = PyList_GetItem(ScilabPythonObject, idObj);
		if (!obj)
		{
			Scierror(999, "No such object on the stack!\n");
			return -1;
		}

		m = PyList_Size(obj);
		item = PyList_GetItem(obj, 0);
		n = PyList_Size(item);
		sciErr = allocMatrixOfDouble(pvApiCtx, pos, m, n, &res);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		for (i = 0; i < m; i++)
		{
			item = PyList_GetItem(obj, i);
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = (double)PyInt_AsLong(PyList_GetItem(item, j));
			}
		}
	}
	#else
	double *res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *obj, *item;

	obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);
	sciErr = allocMatrixOfDouble(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = (double)PyInt_AsLong(PyList_GetItem(item, j));
		}
	}
	#endif
	return 0;
}

#ifdef ____SCILAB_INT64__
int unwrapmatlong(int idObj, int pos)
{
	#ifdef NUMPY
	if (useNumpy)
	{
		PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
		unwrapLongArray(pos, obj);
	}
	else
	{
		long long *res;
		int m, n, i, j;
		SciErr sciErr;
		PyObject *obj, *item;

		obj = PyList_GetItem(ScilabPythonObject, idObj);
		if (!obj)
		{
			Scierror(999, "No such object on the stack!\n");
			return -1;
		}

		m = PyList_Size(obj);
		item = PyList_GetItem(obj, 0);
		n = PyList_Size(item);
		sciErr = allocMatrixOfInteger64(pvApiCtx, pos, m, n, &res);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		for (i = 0; i < m; i++)
		{
			item = PyList_GetItem(obj, i);
			for (j = 0; j < n; j++)
			{
				res[j * m + i] = PyLong_AsLongLong(PyList_GetItem(item, j));
			}
		}
	}
	#else
	long long *res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *obj, *item;

	obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);
	sciErr = allocMatrixOfInteger64(pvApiCtx, pos, m, n, &res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = PyLong_AsLongLong(PyList_GetItem(item, j));
		}
	}
	#endif
	return 0;
}
#endif

int unwrapmatstring(int idObj, int pos)
{
	char **res;
	int m, n, i, j;
	SciErr sciErr;
	PyObject *obj, *item;

	obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	m = PyList_Size(obj);
	item = PyList_GetItem(obj, 0);
	n = PyList_Size(item);

	res = (char **)MALLOC(sizeof(char *) * m * n);
	for (i = 0; i < m; i++)
	{
		item = PyList_GetItem(obj, i);
		for (j = 0; j < n; j++)
		{
			res[j * m + i] = PyString_AsString(PyList_GetItem(item, j));
		}
	}

	sciErr = createMatrixOfString(pvApiCtx, pos, m, n, (char **)res);
	FREE(res);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 0;
}

int createItemInList(int pos, int *tlist_addr, int itemPos, PyObject *obj)
{
	int type = isunwrappable(obj);
	switch (type)
	{
		case -1:;
			return 0;
		case SINGLE_DOUBLE:;
			createdouble(pos, tlist_addr, itemPos, obj);
			return 1;
		case SINGLE_INT:;
			createint(pos, tlist_addr, itemPos, obj);
			return 1;
		case SINGLE_LONG:;
			#ifdef __SCILAB_INT64__
			createlong(pos, tlist_addr, itemPos, obj);
			#endif
			return 1;
		case SINGLE_STRING:;
			createstring(pos, tlist_addr, itemPos, obj);
			return 1;
		case ROW_DOUBLE:;
			createrowdouble(pos, tlist_addr, itemPos, obj);
			return 1;
		case ROW_INT:;
			createrowint(pos, tlist_addr, itemPos, obj);
			return 1;
		case ROW_LONG:;
			#ifdef __SCILAB_INT64__
			createrowlong(pos, tlist_addr, itemPos, obj);
			#endif
			return 1;
		case ROW_STRING:;
			createrowstring(pos, tlist_addr, itemPos, obj);
			return 1;
		case MAT_DOUBLE:;
			creatematdouble(pos, tlist_addr, itemPos, obj);
			return 1;
		case MAT_INT:;
			creatematint(pos, tlist_addr, itemPos, obj);
			return 1;
		case MAT_LONG:;
			#ifdef __SCILAB_INT64__
			creatematlong(pos, tlist_addr, itemPos, obj);
			#endif
			return 1;
		case MAT_STRING:;
			creatematstring(pos, tlist_addr, itemPos, obj);
			return 1;
		case SINGLE_CDOUBLE:;
			createcdouble(pos, tlist_addr, itemPos, obj);
			return 1;
		case ROW_CDOUBLE:;
			createrowcdouble(pos, tlist_addr, itemPos, obj);
			return 1;
		case MAT_CDOUBLE:;
			creatematcdouble(pos, tlist_addr, itemPos, obj);
			return 1;
		#ifdef NUMPY
		case UINT_ARRAY:;
			creatematuint(pos, tlist_addr, itemPos, obj);
			return 1;
		case BYTE_ARRAY:;
			creatematbyte(pos, tlist_addr, itemPos, obj);
			return 1;
		case UBYTE_ARRAY:;
			creatematubyte(pos, tlist_addr, itemPos, obj);
			return 1;
		case SHORT_ARRAY:;
			creatematshort(pos, tlist_addr, itemPos, obj);
			return 1;
		case USHORT_ARRAY:;
			creatematushort(pos, tlist_addr, itemPos, obj);
			return 1;
		case ULONG_ARRAY:;
			#ifdef ____SCILAB_INT64__
			creatematulong(pos, tlist_addr, itemPos, obj);
			#endif
			return 1;
		#endif
	}
}

int unwrapdict(int idObj, int pos)
{
	PyObject *obj, *name, *keys, *key, *vals, *val;
	int nb_item, i, *tlist_addr;
	char **fields;
	obj = PyList_GetItem(ScilabPythonObject, idObj);
	if (!obj)
	{
		Scierror(999, "No such object on the stack!\n");
		return -1;
	}

	nb_item = PyDict_Size(obj);
	keys = PyDict_Keys(obj);
	vals = PyDict_Values(obj);

	name = PyDict_GetItemString(obj, "__tlist_name");
	if (name)
	{
		SciErr err = createTList(pvApiCtx, pos, nb_item, &tlist_addr);
		if (err.iErr)
		{
			printError(&err, 0);
			return 0;
		}
		fields = (char **)MALLOC(nb_item * sizeof(char *));
		fields[0] = PyString_AsString(name);
		for (i = 1; i < nb_item; i++)
		{
			key = PyList_GetItem(keys, i);
			fields[i] = PyString_AsString(key);
		}
		createMatrixOfStringInList(pvApiCtx, pos, tlist_addr, 1, 1, nb_item, fields);
		for (i = 1; i < nb_item; i++)
		{
			val = PyList_GetItem(vals, i);
			createItemInList(pos, tlist_addr, i + 1, val);
		}
		FREE(fields);
		return 0;
	}
	else
	{// to do
		return 0;
	}
}

#ifdef NUMPY
int unwrapmatuint(int idObj, int pos)
{
	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	unwrapUIntArray(pos, obj);
	return 0;
}

int unwrapmatbyte(int idObj, int pos)
{
	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	unwrapByteArray(pos, obj);
	return 0;
}

int unwrapmatubyte(int idObj, int pos)
{
	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	unwrapUByteArray(pos, obj);
	return 0;
}

int unwrapmatshort(int idObj, int pos)
{
	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	unwrapShortArray(pos, obj);
	return 0;
}

int unwrapmatushort(int idObj, int pos)
{
	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	unwrapUShortArray(pos, obj);
	return 0;
}

#ifdef ____SCILAB_INT64__
int unwrapmatulong(int idObj, int pos)
{
	PyObject *obj = PyList_GetItem(ScilabPythonObject, idObj);
	unwrapULongArray(pos, obj);
	return 0;
}
#endif

#endif
