/*
   This file is part of JIMS (http://forge.scilab.org/index.php/p/JIMS/) and
    has been updated for Epfs purposes.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Calixte Denizet, Baozeng Ding.
*/

#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"

#ifdef NUMPY
#include "util.h"
#include "deallocator.h"
#endif

#define FREEMATSTR(mat) for (i = 0; i < row * col ; FREE(mat[i++]));FREE(mat);
#define WRAP(pythontype,data)			\
	if (row == 0 || col == 0)			\
	{								\
		obj = NULL;					\
	}								\
	else if (row == 1 && col == 1)		\
	{								\
		obj = wrapSingle ## pythontype(data[0]);\
	}								\
	else if (row == 1)					\
	{								\
		obj = wrapRow ## pythontype(data, col);\
	}								\
	else								\
	{								\
		obj = wrapMat ## pythontype(data, row, col);\
	}

int createPModule(char *name, int id)
{
	static char* fields[] = {"_PModule", "_id"};
	int* mlistaddr = NULL;
	double moduleId = (double)id;
	SciErr err = createNamedMList(pvApiCtx, name, 2, &mlistaddr);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfStringInNamedList(pvApiCtx, name, mlistaddr, 1, 1, 2, fields);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfDoubleInNamedList(pvApiCtx, name, mlistaddr, 2, 1, 1, (double *)&moduleId);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	return 1;
}

int createPClass(int pos, int id)
{
	static char* fields[] = {"_PClass", "_id"};
	SciErr err;
	int* mlistaddr = NULL;
	
	err = createMList(pvApiCtx, pos, 2, &mlistaddr);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfStringInList(pvApiCtx, pos, mlistaddr, 1, 1, 2, fields);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfDoubleInList(pvApiCtx, pos, mlistaddr, 2, 1, 1, (double *)&id);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	return pos;
}

int createPObject(int pos, int id)
{
	static char* fields[] = {"_PObject", "_id"};
	SciErr err;
	int* mlistaddr = NULL;
	
	err = createMList(pvApiCtx, pos, 2, &mlistaddr);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfStringInList(pvApiCtx, pos, mlistaddr, 1, 1, 2, fields);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfDoubleInList(pvApiCtx, pos, mlistaddr, 2, 1, 1, (double *)&id);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	return pos;
}

PyObject *wrapSingleDouble(double x)
{
	PyObject *object = PyFloat_FromDouble(x);
	return object;
}

PyObject *wrapRowDouble(double *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapDoubleArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("d", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("d", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatDouble(double *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapDoubleArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("d", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("d", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapComplexMatrix(double *cx_real, double *cx_img, int r, int c)
{
	PyObject * object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapCDoubleArray(cx_real, cx_img, r, c);
	}
	else
	{
		int i, j;
		Py_complex new_complex;
		if (r ==1 && c == 1)
		{
			new_complex.real = *cx_real;
			new_complex.imag = *cx_img;
			object = Py_BuildValue("D", &new_complex);
			return object;
		}
		else
		{
			if (r == 1)
			{
				object = PyList_New(c);
				for (i = 0; i < c; i++)
				{
					new_complex.real = cx_real[i];
					new_complex.imag = cx_img[i];
					PyList_SetItem(object, i, Py_BuildValue("D", &new_complex));
				}
			}
			else
			{
				object = PyList_New(r);
				PyObject *row_list;
				for (i = 0; i < r; i++)
				{
					row_list = PyList_New(c);
					for (j = 0; j < c; j++)
					{
						new_complex.real = cx_real[r * j + i];
						new_complex.imag = cx_img[r * j + i];
						PyList_SetItem(row_list, j, Py_BuildValue("D", &new_complex));
					}
					PyList_SetItem(object, i, row_list);
				}
			}
		}
	}
	#else
	int i, j;
	Py_complex new_complex;
	if (r ==1 && c == 1)
	{
		new_complex.real = *cx_real;
		new_complex.imag = *cx_img;
		object = Py_BuildValue("D", &new_complex);
		return object;
	}
	else
	{
		if (r == 1)
		{
			object = PyList_New(c);
			for (i = 0; i < c; i++)
			{
				new_complex.real = cx_real[i];
				new_complex.imag = cx_img[i];
				PyList_SetItem(object, i, Py_BuildValue("D", &new_complex));
			}
		}
		else
		{
			object = PyList_New(r);
			PyObject *row_list;
			for (i = 0; i < r; i++)
			{
				row_list = PyList_New(c);
				for (j = 0; j < c; j++)
				{
					new_complex.real = cx_real[r * j + i];
					new_complex.imag = cx_img[r * j + i];
					PyList_SetItem(row_list, j, Py_BuildValue("D", &new_complex));
				}
				PyList_SetItem(object, i, row_list);
			}
		}
	}
	#endif
	return object;
}

PyObject *wrapSingleInt(int x)
{
	PyObject *object = PyInt_FromLong(x);
	return object;
}

PyObject *wrapRowInt(int *x, int len)
{
	PyObject * object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapIntArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatInt (int *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapIntArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapSingleUInt(unsigned int x)
{
	PyObject *object = PyInt_FromLong(x);
	return object;
}

PyObject *wrapRowUInt(unsigned int *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapUIntArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatUInt(unsigned int *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapUIntArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapSingleByte(char x)
{
	PyObject *object = PyInt_FromLong(x);
	return object;
}

PyObject *wrapRowByte(char *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapByteArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatByte(char *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapByteArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapSingleUByte(unsigned char x)
{
	PyObject *object = Py_BuildValue("i", x);
	return object;
}

PyObject *wrapRowUByte(unsigned char *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapUByteArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("i", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatUByte(unsigned char *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapUByteArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("i", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapSingleShort(short x)
{
	PyObject *object = Py_BuildValue("h", x);
	return object;
}

PyObject *wrapRowShort(short *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapShortArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("h", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("h", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatShort(short *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapShortArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("h", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("h", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapSingleUShort(unsigned short x)
{
	PyObject *object = Py_BuildValue("h", x);
	return object;
}

PyObject *wrapRowUShort(unsigned short *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapUShortArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("h", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("h", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatUShort(unsigned short *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapUShortArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("h", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("h", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapSingleString(char *x)
{
	PyObject *object = Py_BuildValue("s", x);
	return object;
}

PyObject *wrapRowString(char **x, int len)
{
	int i;
	PyObject *object = PyList_New(len);
	for (i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("s", x[i]));
	}
	return object;
}

PyObject *wrapMatString(char **x, int r, int c)
{
	int i, j;
	PyObject *object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("s", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	return object;
}

#ifdef __SCILAB_INT64__
PyObject *wrapSingleLong(long long x)
{
	PyObject *object = PyLong_FromLongLong(x);
	return object;
}

PyObject *wrapRowLong(long long *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapLongArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, Py_BuildValue("L", x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, Py_BuildValue("L", x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatLong(long long *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapLongArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, Py_BuildValue("L", x[r * j + i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, Py_BuildValue("L", x[r * j + i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}

PyObject *wrapSingleULong(unsigned long long x)
{
	PyObject *object = PyLong_FromUnsignedLongLong(x);
	return object;
}

PyObject *wrapRowULong(unsigned long long *x, int len)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapULongArray(x, 1, len);
	}
	else
	{
		int i;
		object = PyList_New(len);
		for(i = 0; i < len; i++)
		{
			PyList_SetItem(object, i, PyLong_FromUnsignedLongLong(x[i]));
		}
	}
	#else
	int i;
	object = PyList_New(len);
	for(i = 0; i < len; i++)
	{
		PyList_SetItem(object, i, PyLong_FromUnsignedLongLong(x[i]));
	}
	#endif
	return object;
}

PyObject *wrapMatLong(unsigned long long *x, int r, int c)
{
	PyObject *object;
	#ifdef NUMPY
	if (useNumpy)
	{
		object = wrapULongArray(x, r, c);
	}
	else
	{
		int i, j;
		object = PyList_New(r);
		PyObject *row_list;
		for (i = 0; i < r; i++)
		{
			row_list = PyList_New(c);
			for (j = 0; j < c; j++)
			{
				PyList_SetItem(row_list, j, PyLong_FromUnsignedLongLong(x[i]));
			}
			PyList_SetItem(object, i, row_list);
		}
	}
	#else
	int i, j;
	object = PyList_New(r);
	PyObject *row_list;
	for (i = 0; i < r; i++)
	{
		row_list = PyList_New(c);
		for (j = 0; j < c; j++)
		{
			PyList_SetItem(row_list, j, PyLong_FromUnsignedLongLong(x[i]));
		}
		PyList_SetItem(object, i, row_list);
	}
	#endif
	return object;
}
#endif

PyObject *convArg(int *addr)
{
	SciErr err;
	int *child = NULL;
	int i;
	int typ, row = 0, col = 0, rc;
	char **matS;
	PyObject *obj;

	err = getVarType(pvApiCtx, addr, &typ);
	if (err.iErr)
	{
		printError(&err, 0);
		return -1;
	}

	switch (typ)
	{
		case sci_matrix :;
			if (isVarComplex(pvApiCtx, addr))
			{
				double *cx_real, *cx_img;
				err = getComplexMatrixOfDouble(pvApiCtx, addr, &row, &col, &cx_real, &cx_img);
				if (err.iErr)
				{
					printError(&err, 0);
					return -1;
				}

				obj = wrapComplexMatrix(cx_real, cx_img, row, col);
				return obj;
			}

			double * mat;
			err = getMatrixOfDouble(pvApiCtx, addr, &row, &col, &mat);
			if (err.iErr)
			{
				printError(&err, 0);
				return -1;
			}

			WRAP(Double, mat);
			return obj;

		case sci_ints :;
			int prec;
			void *ints;

			err = getMatrixOfIntegerPrecision(pvApiCtx, addr, &prec);
			if (err.iErr)
			{
				printError(&err, 0);
				return -1;
			}

			switch (prec)
			{
				case SCI_INT8 :;
					err = getMatrixOfInteger8(pvApiCtx, addr, &row, &col, (char**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(Byte, ((char*)ints));
					return obj;

				case SCI_UINT8 :;
					err = getMatrixOfUnsignedInteger8(pvApiCtx, addr, &row, &col, (unsigned char**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(UByte, ((unsigned char*)ints));
					return obj;

				case SCI_INT16 :;
					err = getMatrixOfInteger16(pvApiCtx, addr, &row, &col, (short**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(Short, ((short*)ints));
					return obj;

				case SCI_UINT16 :;
					err = getMatrixOfUnsignedInteger16(pvApiCtx, addr, &row, &col, (unsigned short**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(UShort, ((unsigned short*)ints));
					return obj;

				case SCI_INT32 :;
					err = getMatrixOfInteger32(pvApiCtx, addr, &row, &col, (int**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(Int, ((int*)ints));
					return obj;

				case SCI_UINT32 :;
					err = getMatrixOfUnsignedInteger32(pvApiCtx, addr, &row, &col, (unsigned int**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(UInt, ((unsigned int*)ints));
					return obj;

			#ifdef __SCILAB_INT64__
				case SCI_INT64 :;
					err = getMatrixOfInteger64(pvApiCtx, addr, &row, &col, (long long**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(Long, ((long long*)ints));
					return obj;

				case SCI_UINT64 :;
					err = getMatrixOfUnsignedInteger64(pvApiCtx, addr, &row, &col, (unsigned long long**)(&ints));
					if (err.iErr)
					{
						printError(&err, 0);
						return -1;
					}

					WRAP(ULong, ((unsigned long long*)ints));
					return obj;
			#endif
			}

		case sci_strings :;
			matS = getStrings(addr, &row, &col);

			WRAP(String, matS);
			FREEMATSTR(matS);
			return obj;

		break;
		default :;
			Scierror(999, "Wrong type for input argument\n");
			return NULL;
	}
}

/**
 * This function convers the tlist to a Python dictionary.
 *
 * A tlist x = tlist(['test','a','b'],12,'item')
 * is transformed in python in
 * x = { "__tlist_name" : "test",
 *       "a" : 12,
 *       "b" : "item",
 *     }
 *
 * @param tlist_address: the address of the scilab variable we want to read
 * @return: a dictionary
*/
PyObject *wrapList(int *tlist_address)
{
	SciErr err;
	PyObject *new_dict;
	int nb_item = 0, i, row, col;
	char **keys;

	err = getListItemNumber(pvApiCtx, tlist_address, &nb_item);
	if (err.iErr)
	{
		printError(&err, 0);
		return NULL;
	}
	new_dict = PyDict_New();
	for (i = 1; i <= nb_item; ++i)
	{
		int *item_address;

		err = getListItemAddress(pvApiCtx, tlist_address, i, &item_address);
		if (err.iErr)
		{
			printError(&err, 0);
			return NULL;
		}

		if (i == 1)
		{
			keys = getStrings(item_address, &row, &col);
			if (row !=1 || col != nb_item)
			{
				Scierror(999, "Cannot deal with the tlist\n");
				return NULL;
			}
			PyDict_SetItemString(new_dict, "__tlist_name", Py_BuildValue("s", keys[0]));
		}
		else
		{
			PyObject *item = convArg(item_address);
			if (item == NULL)
			{
				Scierror(999, "Cannot convert the tlist\n");
				return NULL;
			}
			PyDict_SetItemString(new_dict, keys[i-1], item);
		}
	}
	FREEMATSTR(keys);

	return new_dict;
}
